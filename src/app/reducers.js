import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import userReducer from "./userProfile/reducers/userReducer";
export default combineReducers({
  product: productReducer,
  user:userReducer
});