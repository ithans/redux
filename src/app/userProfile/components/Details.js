import React from 'react';

const Details = ({name, gender, description}) => {
  return (<div>
    <div>
      <label>UserName: </label>
      <span>{name}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
    <div>
      <label>Description: </label>
      <span>{description}</span>
    </div>
  </div>)
};

export default Details;