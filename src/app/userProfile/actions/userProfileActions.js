export const getUserDetails = (id) => (dispatch) => {
  const URI="http://localhost:8080/api/user-profiles/"+id;
  console.log(URI);
  fetch('http://localhost:8080/api/user-profiles/' + id)
    .then(response => response.json())
    .then(result => {
      dispatch({
        type: 'GET_USER_DETAILS',
        userDetails: result
      });
    })
};
