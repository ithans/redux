import React, {Component} from 'react';
import {connect} from "react-redux";
import {getUserDetails} from '../actions/userProfileActions';
import Details from "../components/Details";
import {bindActionCreators} from "redux";

class UserDetails extends Component {
  componentDidMount() {
    this.props.getUserDetails(this.props.match.params.id);
  }

  render() {
    const {name, gender, description} = this.props.userDetails;
    return (
      <div className="">
        <h3>User Profile</h3>
        <Details name={name} gender={gender} description={description} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userDetails: state.user.userDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getUserDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
