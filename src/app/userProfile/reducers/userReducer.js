const initState = {
  userDetails: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_USER_DETAILS':
      return {
        ...state,
        userDetails: action.userDetails
      };

    default:
      return state
  }
};
